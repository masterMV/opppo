//
//  Deque.hpp
//  lab1
//
//  Created by maxim vingalov on 18/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#ifndef Deque_hpp
#define Deque_hpp

#include "Entry.hpp"
#include "Matrix.h"

class Deque {
private:
    Entry* entries;
    int capacity;
    int size;
    int offset;
    
    int getEntry(int index);
    int getOffsetInEntry(int index);
    int numEntries();
    void checkBoundaries(int index);
    void allocMemory();

public:
    Deque();
    void insertFirst(Matrix* value);
    void append(Matrix* value);
    void removeLast();
    void removeFirst();
    Matrix* get(int index);
    int count();
    
    void insert(Matrix* value, int index);
    void removeAt(int index);

};

#endif /* Deque_hpp */

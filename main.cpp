//
//  main.cpp
//  lab1
//
//  Created by maxim vingalov on 18/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include "Deque.hpp"
#include "Matrix.h"

void read(Deque& deque) {
    
    std::ifstream in = std::ifstream("in.txt");
    std::string line;
    std::string temp;
    
    int number = 0, size = 0, i = 0;
    Matrix* mat = nullptr;
    
    MatrixType type = MatrixType::none;

    if (in.is_open()) {
        
        //omit first line, cause this line contains comments
        getline(in, line);
        
        while (getline(in, line)) {
            
            std::stringstream ss(line);
            
            while (!ss.eof()) {
                ss >> temp;
                if (std::stringstream(temp) >> number) {
                    
                    if (type == MatrixType::none) {
                        
                        if (number == 1) type = MatrixType::oneD;
                        else if (number == 2) type = MatrixType::twoD;
                        break;
                        
                    }
                    
                    if (size == 0) {
                        size = number;
                        break;
                    }
                    
                    if (i == 0 && size > 0 && type != MatrixType::none) {
                        mat = new Matrix();
                        mat->type = type;
                        mat->size = size;
                        
                        if (type == MatrixType::oneD) {
                            mat->oneDimensional = new int[size];
                        } else {
                            int rows = sqrt(size);
                            mat->twoDimensional = new int*[rows];
                            for (int i = 0; i < rows; i++) {
                                mat->twoDimensional[i] = new int[rows];
                            }
                        }

                    }
                    
                    if (size > 0 && type != MatrixType::none) {
                        if (type == MatrixType::oneD) {
                            mat->oneDimensional[i] = number;
                        } else {
                            int rows = sqrt(size);
                            mat->twoDimensional[i / rows][i % rows] = number;
                        }
                        i++;
                        
                        //if data ends -> append matrix to deque
                        if (i == size) {
                            deque.append(mat);
                            i = 0;
                            size = 0;
                            type = MatrixType::none;
                        }
                    }
                }
                temp = "";
            }
            
        }
        
    } else {
        std::cout << "Can't open such file" << std::endl;
        exit(1);
    }
    
}

int main(int argc, const char * argv[]) {
    
    Deque deque = Deque();
    
    read(deque);
    
    for (int i = 0; i < deque.count(); i++) {
        
        int size = deque.get(i)->size;
        int halfSize = sqrt(size);

        if (deque.get(i)->type == MatrixType::oneD) {
            
            std::cout << "\nvalues in one dimensional array" << std::endl;
            
            for (int j = 0; j < size; j++) {
                if (j % halfSize == 0) std::cout << std::endl;
                std::cout << deque.get(i)->oneDimensional[j] << " ";
            }

        } else {
            
            std::cout << "\nvalues in two dimensional array" << std::endl;
            for (int j = 0; j < halfSize; j++) {
                for (int k = 0; k < halfSize; k++) {
                    std::cout << deque.get(i)->twoDimensional[j][k] << " ";
                }
                std::cout << std::endl;
            }

        }

    }
    
    return 0;
}

//
//  Entry.cpp
//  lab1
//
//  Created by maxim vingalov on 18/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#include "Entry.hpp"

Entry::Entry() {
    elems = new Matrix[CHUNK_SIZE];
}

void Entry::addElem(int index, Matrix* value) {
    elems[index] = *value;
}

Matrix* Entry::getElem(int index) {
    return elems + index;
//    return elems[index];
}

//
//  Deque.cpp
//  lab1
//
//  Created by maxim vingalov on 18/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#include "Deque.hpp"
#include <iostream>
#include <stdlib.h>

Deque::Deque() {
    entries = new Entry();
    capacity = CHUNK_SIZE;
    size = 0;
    offset = 0;
}

int Deque::getEntry(int index) {
    return (offset + index) / CHUNK_SIZE;
}

int Deque::getOffsetInEntry(int index) {
    return (offset + index) % CHUNK_SIZE;
}

int Deque::numEntries() {
    return capacity / CHUNK_SIZE;
}

void Deque::checkBoundaries(int index) {
    
    if (index < 0 || index >= size) {
        std::cout << "out of bound" << std::endl;
        exit(1);
    }

}


void Deque::insert(Matrix* value, int index) {
    
    if (size == 0) {
        std::cout << "out of bound" << std::endl;
        exit(1);
    }
    
    checkBoundaries(index);

    if (size + 1 >= capacity) {
        allocMemory();
    }
    
    for (int i = size; i > index; i--) {
        entries[getEntry(i)].addElem(getOffsetInEntry(i), get(i - 1));
    }
    
    entries[getEntry(index)].addElem(getOffsetInEntry(index), value);

    size++;
    
}

void Deque::removeAt(int index) {

    if (size == 0) {
        std::cout << "out of bound" << std::endl;
        exit(1);
    }
    
    checkBoundaries(index);
    
    for (int i = index; i < size - 1; i++) {
        entries[getEntry(i)].addElem(getOffsetInEntry(i), get(i + 1));
    }
    
    size--;

}

void Deque::allocMemory() {
    
    Entry* newEntry = new Entry();
    entries = (Entry*)realloc(entries, sizeof(Entry) * numEntries() + 1);
    memcpy(entries + numEntries(), newEntry, sizeof(Entry));
    capacity += CHUNK_SIZE;

}

void Deque::append(Matrix* value) {
    if (size < capacity) {
        entries[getEntry(size)].addElem(getOffsetInEntry(size), value);
    } else {
        
        allocMemory();
        entries[getEntry(size)].addElem(getOffsetInEntry(size), value);
        
    }
    size++;
}

Matrix* Deque::get(int index) {
    checkBoundaries(index);
    return entries[getEntry(index)].getElem(getOffsetInEntry(index));
}

void Deque::removeLast() {
    if (size > 0) {
        size--;
    }
}

void Deque::insertFirst(Matrix* value) {
    if (offset == 0) {
        
        Entry* newEntry = new Entry();

        entries = (Entry*)realloc(entries, sizeof(Entry) * numEntries() + 1);
        
        memcpy(entries + 1, entries, sizeof(Entry) * numEntries());
        memcpy(entries, newEntry, sizeof(Entry));
        
        offset = CHUNK_SIZE - 1;
        
        entries[0].addElem(offset, value);
        capacity += CHUNK_SIZE;
    } else {
        offset--;
        entries[0].addElem(offset, value);
    }
    size++;
}

void Deque::removeFirst() {
    if (offset < capacity) {
        offset++;
        size--;
    }
}

int Deque::count() {
    return size;
}


//
//  Entry.hpp
//  lab1
//
//  Created by maxim vingalov on 18/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#ifndef Entry_hpp
#define Entry_hpp

#include "Matrix.h"

#define CHUNK_SIZE 10

class Entry {
    Matrix* elems;
public:
    Entry();
    void addElem(int index, Matrix* value);
//    int getElem(int);
    Matrix* getElem(int index);
    
};


#endif /* Entry_hpp */

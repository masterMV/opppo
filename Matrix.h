//
//  Matrix.h
//  PPO
//
//  Created by maxim vingalov on 31/01/2019.
//  Copyright © 2019 maxim vingalov. All rights reserved.
//

#ifndef Matrix_h
#define Matrix_h

enum MatrixType {
    oneD, twoD, none
};

struct Matrix {
    int** twoDimensional;
    int* oneDimensional;
    int size;
    MatrixType type;
};


#endif /* Matrix_h */
